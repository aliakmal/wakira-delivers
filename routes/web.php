<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  if(\Auth::check()){
    return redirect()->to('/home');
  }
  return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('businesses', 'BusinessesController');
Route::resource('venues', 'VenuesController');
Route::resource('cities', 'CitiesController');
Route::get('venues/{id}/timings', array('uses'=>'VenuesController@getTimings', 'as'=>'venues.timings.get'));
Route::post('venues/{id}/timings', array('uses'=>'VenuesController@postTimings', 'as'=>'venues.timings.post'));

Route::get('venues/{id}/delivery_locations', array('uses'=>'VenuesController@getDeliveryLocations', 'as'=>'venues.delivery_locations.get'));
Route::post('venues/{id}/delivery_locations', array('uses'=>'VenuesController@postDeliveryLocations', 'as'=>'venues.delivery_locations.post'));

