@extends('layouts.app')
@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Users</a></li>
    <li class="breadcrumb-item active" aria-current="page">New User</li>
  </ol>
</nav>
  <div class="card">

      <div class="card-body">

    <h1>Create User</h1>

{{ Form::open(array('route' => 'users.store', 'files'=>true, 'class' => ' ')) }}

  <div class="form-group row">
    {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Full Name')) }}
    </div>
  </div>

  <div class="form-group row">
    {{ Form::label('email', 'Email:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('email', Input::old('email'), array('class'=>'form-control', 'placeholder'=>'Email Address')) }}
    </div>
  </div>

  <div class="form-group row">
    {{ Form::label('Password', 'Password:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::password('password', Input::old('password'), array('class'=>'form-control', 'placeholder'=>'Password')) }}
    </div>
  </div>

  <div class="form-group row">
    {{ Form::label('role', 'Role:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('role',  $roles,   Input::old('role'), array('class'=>'form-control')) }}
    </div>
  </div>


  <div class="form-group row">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
  </div>

{{ Form::close() }}
</div>
</div>

@stop