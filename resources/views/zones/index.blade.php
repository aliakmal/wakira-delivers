@extends('layouts.application')
@section('content')
<div class="card">
  <div class="card-body">

    <h1>Business Entities</h1>

    <p>{{ link_to_route('businesses.create', 'Add New Business Entity', null, array('class' => 'btn btn-lg btn-success')) }}</p>

    @if ($businesses->count())
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Venues</th>
            <th>&nbsp;</th>
          </tr>
        </thead>

        <tbody>
          @foreach ($businesses as $business)
            <tr>
              <td>{{{ $business->name }}}</td>
              <td>{{{ $business->type }}}</td>
              <td>{{{ $business->venues->count() }}} venues</td>
              <td>
                {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('businesses.destroy', $business->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
                {{ link_to_route('businesses.edit', 'Edit', array($business->id), array('class' => 'btn btn-info')) }}
                {{ link_to_route('businesses.show', 'Show', array($business->id), array('class' => 'btn btn-info')) }}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>

      <div class="row">
        <div class="col-md-12">
          {{ $businesses->render() }}
          <div class="pull-right">
            {{ count($businesses) }} / {{ $businesses->total() }} entries
          </div>
        </div>
      </div>

    @else
      There are no business entities
    @endif
  </div>
</div>
@stop
