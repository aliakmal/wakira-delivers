@extends('layouts.application')
@section('content')
<div class="card">
  <div class="card-body">
    <h1>{{ $business->name }}</h1>
    <hr/>
    <p><b>{{ $business->type }}</b></p>
    {{ $business->desc }}
    @if(count($business->venues)>0)
    <div class="card">
      <div class="card-header">
        Venues
        <a href="{{ route('venues.create', array('bid'=>$business->id)) }}" class="pull-right">Add Venue</a>
      </div>
      <div class="card-body">
        <table class="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>address</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach($business->venues as $venue)
              <tr>
                <th>{{ $venue->id }}</th>
                <th>{{ $venue->name }}</th>
                <th>{{ $venue->getFormattedAddress() }}</th>
                <th>
                  @if($venue->hasDelivery())
                    <span class="label label-success">DELIVERY</span>
                  @else
                    <span class="label label-danger">NO DELIVERY</span>
                  @endif
                </th>
                <th>
                  <a href="{{ route('venues.edit', $venue) }}">Edit</a>
                  <a href="{{ route('venues.show', $venue) }}">Show</a>
                </th>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    @else
      <div class="alert alert-warning">
        <a href="{{ route('venues.create', array('bid'=>$business->id)) }}" class="pull-right">Add Venue</a>
      </div>
    @endif

    {{ link_to_route('businesses.edit', 'Edit', $business->id, array('class' => 'btn btn-lg btn-default')) }}
    {{ link_to_route('businesses.index', 'Return', $business->id, array('class' => 'btn btn-lg btn-default')) }}
  </div>
</div>
@stop
