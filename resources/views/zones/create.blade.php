@extends('layouts.application')
@section('content')
            <div class="card">

                <div class="card-body">

<h1>Create Business Entitie</h1>
<hr/>

{{ Form::open(array('route' => 'businesses.store', 'files'=>true, 'class' => ' ')) }}

  <div class="form-group row">
    {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
    </div>
  </div>
  <div class="form-group row">
    {{ Form::label('type', 'Type:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
    {{ Form::select('type',  ['cafe'=>'Cafe', 'restaurant'=>'Restaurant'],   Input::old('type'),array('class'=>'form-control', 'placeholder'=>'Select Business Type')) }}
    </div>
  </div>
  <div class="form-group row">
    {{ Form::label('desc', 'Description:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('desc', Input::old('desc'), array('class'=>'form-control', 'placeholder'=>'Description')) }}
    </div>
  </div>

  <div class="form-group row">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
  </div>

{{ Form::close() }}
</div>
</div>
@stop
