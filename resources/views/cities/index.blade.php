@extends('layouts.application')
@section('content')
<div class="card">
  <div class="card-body">

    <h1>Cities</h1>


    @if ($cities->count())
      <table class="table table-striped">

        <tbody>
          @foreach ($cities as $city)
            <tr>
              <td>
                {{ link_to_route('cities.show', $city->name, $city->id) }}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>


    @endif
  </div>
</div>
@stop
