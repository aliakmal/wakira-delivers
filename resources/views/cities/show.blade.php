@extends('layouts.application')
@section('content')
<div class="card">
  <div class="card-body">
    <h1>{{ $city->name }}</h1>
    <hr/>
    @if(count($city->zones)>0)
    <div class="card">
      <div class="card-body">
        <table class="table">
          <tbody>
            @foreach($city->zones as $zone)
              <tr>
                <th>{{ $zone->name }}</th>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    @endif

    {{ link_to_route('cities.index', 'Return', null, array('class' => 'btn btn-lg btn-default')) }}
  </div>
</div>
@stop
