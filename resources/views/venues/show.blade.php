@extends('layouts.app')
@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{ route('businesses.index') }}">Businesses</a></li>
    <li class="breadcrumb-item"><a href="{{ route('businesses.show', ['id'=>$venue->business_id]) }}">{{$venue->business->name}}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $venue->name}}</li>
  </ol>
</nav>

<div class="card">
  <div class="card-body">
    <h1>{{ $venue->name }}</h1>
    <hr/>
    @if($venue->hasDelivery())
      <span class="badge badge-pill badge-success">DELIVERY</span>
    @else
      <span class="badge badge-pill badge-danger">NO DELIVERY</span>
    @endif

    <div>{{ $venue->getFormattedAddress() }}</div>

  </div>
  <div class="card-footer">
    {{ link_to_route('venues.edit', 'Edit', $venue->id, array('class' => 'btn  btn-primary')) }}
    {{ link_to_route('businesses.show', 'Return', $business->id, array('class' => 'btn  btn-primary')) }}
  </div>
</div>
<br/>
<br/>
<div class="card">
  <div class="card-body">
    <h1>{{ $venue->name }} Timings</h1>
    @foreach($venue->timings as $timing)
      <div class="row">
        <div class="col-md-6">
          {{$timing->days_of_week}}
        </div>
        <div class="col-md-6">
          {{$timing->open}} to
          {{$timing->close}}
        </div>
      </div>

    @endforeach
  </div>
  <div class="card-footer">

    {{ link_to_route('venues.timings.get', 'Edit', $venue->id, array('class' => 'btn  btn-primary')) }}
  </div>
</div>
<br/>
<br/>

<div class="card">
  <div class="card-body">
    <h1>{{ $venue->name }} Delivery Locations</h1>
    @foreach($venue->delivery_locations as $delivery_location)
      <div class="row">
        <div class="col-md-6">
          {{$delivery_location->zone->name}}
        </div>
      </div>
    @endforeach
  </div>
  <div class="card-footer">

    {{ link_to_route('venues.delivery_locations.get', 'Edit', $venue->id, array('class' => 'btn  btn-primary')) }}
  </div>
</div>

@stop
