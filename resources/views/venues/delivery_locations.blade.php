@extends('layouts.app')
@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{ route('businesses.index') }}">Businesses</a></li>
    <li class="breadcrumb-item"><a href="{{ route('businesses.show', ['id'=>$venue->business_id]) }}">{{$venue->business->name}}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('venues.show', ['id'=>$venue->id]) }}">{{$venue->name}}</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Delivery Locations</li>
  </ol>
</nav>
<div class="card">
  <div class="card-body">
    <h1>Edit {{ $venue->name }} Delivery Locations</h1>

    {{ Form::model($venue, array('class' => 'form-horizontal', 'files'=>true, 'method' => 'POST')) }}

      <div class="form-group row">
        <div class="col-sm-12">
          {{ Form::select('delivery_locations[]',  \App\Zone::select()->pluck('name', 'id')->all(),   $venue->delivery_locations()->pluck('zone_id','zone_id')->all(), array('class'=>'form-control', 'style'=>'height:400px', 'multiple'=>'multiple')) }}
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-2 control-label">&nbsp;</label>
        <div class="col-sm-10">
          {{ Form::submit('Edit', array('class' => 'btn btn-lg btn-primary')) }}
        </div>
      </div>

    {{ Form::close() }}
  </div>
</div>
@stop
