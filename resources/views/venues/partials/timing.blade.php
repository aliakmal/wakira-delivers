<?php $replace = '';?>
<?php 
$days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
if(isset($timing)):
    $replace = $timing->id;
else:
    $replace = '_REPLACE_';
endif;?>
<div class="row content">
    <div class=" col-md-6">
        <div class="btn-group toggles " data-toggle="buttons">
            @for($i=0; $i < 7; $i++)
                <?php
                $checked = false;
                if(isset($timing)){
                    $cday = $days[$i];
                    $checked = strstr($timing->days_of_week, $cday);
                }
                    
                
                ?>
              <label class="btn btn-default {{ $checked ? 'active' :'' }} ">
                <input type="checkbox"  checked="{{ $checked ? 'checked' :'' }}" value=" {{ $days[$i] }} " /> {{ $days[$i]}}
              </label>
            @endfor
            <input name="timings[{{ $replace }}][days_of_week]" class="days_of_week" value="{{ isset($timing)? $timing->days_of_week : '' }}" type="hidden" />
        </div>
    </div>
    <?php 
        $times = array();

        for($i=0;$i<24; $i++){
            foreach(['00', '15', '30', '45'] as $mts){
                $t = str_pad($i, 2, 0, STR_PAD_LEFT).':'.$mts;
                $times[$t] = $t;
            }
        }

        $t = '23:59';
        $times[$t] = $t;
    ?>

    <div class=" col-md-2">
        {{ Form::select("timings[$replace][open]", $times, isset($timing)?$timing->open:Input::old('open'), array('class'=>'form-control timepicker', 'placeholder'=>'Open')) }}
    </div>
    <div class=" col-md-2">
        <p>{{ Form::select("timings[$replace][close]", $times,  isset($timing)?$timing->close:Input::old('close'), array('class'=>'form-control timepicker', 'placeholder'=>'Close')) }}</p>
    </div>
@if(isset($timing))
    {{ Form::hidden("timings[$replace][id]", $timing->id) }}
@endif
    <div class=" col-md-2">
@if(isset($timing))
    {{ Form::checkbox("timings[$replace][deletable]", $timing->id, false, array('style'=>'display:none;', 'class'=>'deletable') ) }}
    <a href="javascript:void(0)" class="delete-existing btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
@else
    <a href="javascript:void(0)" class="delete btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
@endif
    </div>

</div>
