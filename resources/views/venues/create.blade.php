@extends('layouts.app')
@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{ route('businesses.index') }}">Businesses</a></li>
    <li class="breadcrumb-item"><a href="{{ route('businesses.show', ['id'=>$business->id]) }}">{{$business->name}}</a></li>
    <li class="breadcrumb-item active" aria-current="page">New Venue</li>
  </ol>
</nav>
            <div class="card">

                <div class="card-body">

<h1>Create Venue for {{ $business->name }}</h1>
<hr/>

{{ Form::open(array('route' => 'venues.store', 'files'=>true, 'class' => ' ')) }}

  <div class="form-group row">
    {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Name')) }}
    </div>
  </div>
  <div class="form-group row">
    {{ Form::label('address_1', 'Address Line 1:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('address_1', Input::old('address_1'), array('class'=>'form-control', 'placeholder'=>'Address Line 1')) }}
    </div>
  </div>
  <div class="form-group row">
    {{ Form::label('address_2', 'Address Line 2:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('address_2', Input::old('address_2'), array('class'=>'form-control', 'placeholder'=>'Address Line 2')) }}
    </div>
  </div>
  <div class="form-group row">
    {{ Form::label('city', 'City:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('city', Input::old('city'), array('class'=>'form-control', 'placeholder'=>'City')) }}
    </div>
  </div>
  <div class="form-group row">
    {{ Form::label('state', 'State:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('state',  \App\City::select()->pluck('name', 'name')->all(),   Input::old('state'),array('class'=>'form-control', 'placeholder'=>'Select State')) }}
    </div>
  </div>
  <div class="form-group row">
    {{ Form::label('country', 'Country:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('country',  ['AE'=>'United Arab Emirates'],   Input::old('state'),array('class'=>'form-control')) }}
    </div>
  </div>
  <div class="form-group row">
    {{ Form::label('has_delivery', 'Has Delivery:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
    {{ Form::select('has_delivery',  ['0'=>'No Delivery', '1'=>'Delivery Available'],   Input::old('has_delivery'),array('class'=>'form-control')) }}
    </div>
  </div>
  
  {{ Form::hidden('business_id', $business->id) }}
  <div class="form-group row">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
  </div>

{{ Form::close() }}
</div>
</div>
@stop
