@extends('layouts.app')
@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{ route('businesses.index') }}">Businesses</a></li>
    <li class="breadcrumb-item"><a href="{{ route('businesses.show', ['id'=>$venue->business_id]) }}">{{$venue->business->name}}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('venues.show', ['id'=>$venue->id]) }}">{{$venue->name}}</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Timings</li>
  </ol>
</nav>

<div class="card">
  <div class="card-body">
    <h1>Timings for {{ $venue->name }}</h1>
    <hr/>
    <div class="row">
      <div class="col-md-10 ">
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              {{ implode('', $errors->all('<li class="error">:message</li>')) }}
            </ul>
          </div>
        @endif
      </div>
    </div>
    @include('venues.partials.timings')
    {{ link_to_route('venues.show', 'Return', array($venue->id), array('class' => 'btn btn-primary')) }}
  </div>
</div>

@stop
