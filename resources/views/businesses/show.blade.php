@extends('layouts.app')
@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="{{ route('businesses.index') }}">Businesses</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $business->name}}</li>
  </ol>
</nav>
<div class="card">
  <div class="card-body">
    <h1>{{ $business->name }}</h1>
    <hr/>
    <dl class="dl-vertical">
      <dt>Type</dt>
      <dd>{{ ucwords($business->type) }}</dd>
      <dt>Desc</dt>
      <dd>{{ $business->desc }}</dd>
    </dl>

    @if(count($business->venues)>0)
    <div class="card">
      <div class="card-header">
        Venues
        <a href="{{ route('venues.create', array('bid'=>$business->id)) }}" class="btn btn-primary float-right">Add Venue</a>
      </div>
      <div class="card-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>address</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach($business->venues as $venue)
              <tr>
                <th>{{ $venue->id }}</th>
                <th>{{ $venue->name }}</th>
                <th>{{ $venue->getFormattedAddress() }}</th>
                <th>
                  @if($venue->hasDelivery())
                    <span class="badge badge-success">DELIVERY</span>
                  @else
                    <span class="badge badge-danger">NO DELIVERY</span>
                  @endif
                </th>
                <th>
                  <a href="{{ route('venues.edit', $venue) }}" class="btn btn-light"><i class="fa fa-pen"></i></a>
                  <a href="{{ route('venues.show', $venue) }}" class="btn btn-light"><i class="fa fa-eye"></i></a>
                </th>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    @else
      <div class="alert alert-warning">
        <a href="{{ route('venues.create', array('bid'=>$business->id)) }}" class="pull-right">Add Venue</a>
      </div>
    @endif

    {{ link_to_route('businesses.edit', 'Edit', $business->id, array('class' => 'btn btn-lg btn-default')) }}
    {{ link_to_route('businesses.index', 'Return', $business->id, array('class' => 'btn btn-lg btn-default')) }}
  </div>
</div>
@stop
