@extends('layouts.app')

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
    <li class="breadcrumb-item active" aria-current="page">Businesses</li>
  </ol>
</nav>
<div class="card">
  <div class="card-body">
    @if (session('status'))
      <div class="alert alert-success">
          {{ session('status') }}
      </div>
    @endif
    <h1>Business Entities</h1>
    @if ($businesses->count())
      <table class="table ">
        <thead>
          <tr>
            <th>Name</th>
            <th>Type</th>
            <td title="Accepts Online Payment"><i class="fa text-muted fa-credit-card"></i></td>
            <td title="Is Business Active"><i class="fa text-muted fa-check"></i></td>
            <td title="Number of Menus"><i class="fa text-muted fa-file"></i></td>
            <th></th>
          </tr>
        </thead>

        <tbody>
          @foreach ($businesses as $business)
            <tr>
                <td>
                {{{ $business->name }}}{{{ count($business->venues)>0?'('.count($business->venues).')':'' }}}
                </td>
            <td>{{{ ucwords($business->type) }}}</td>
            <td><i class="fa text-success fa-circle"></i></td>
            <td><i class="fa text-success fa-circle"></i></td>
            <td>3</td>
            <td>
                <!--{{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('businesses.destroy', $business->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}-->
                {{ link_to_route('businesses.edit', 'Edit', array($business->id), array('class' => 'btn btn-info')) }}
                {{ link_to_route('businesses.show', 'Access', array($business->id), array('class' => 'btn btn-info')) }}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
      <p>{{ link_to_route('businesses.create', 'Add New Business Entity', null, array('class' => 'btn btn-primary')) }}</p>
    @else
      <div class="alert alert-warning">
        <p>
          You have no business entities set up
          {{ link_to_route('businesses.create', 'Click here', null, array('class' => 'btn btn-sm btn-primary')) }}
          to create a new entity
        </p>
      </div>
    @endif




                </div>
            </div>
@endsection
