@extends('layouts.app')

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
  </ol>
</nav>
<div class="card">
  <div class="card-body">
    @if (session('status'))
      <div class="alert alert-success">
          {{ session('status') }}
      </div>
    @endif
    <div class="row">
      <div class="col-md-4">
        <div class="card card-primary">
          <div class="card-body">
            <i class="fa float-left fa-4x pr-5 fa-building"></i><p>Business Entities/Restaurants</p>
          </div>
          <div class="card-footer">
            <a href="{{ route('businesses.index') }}" class="btn btn-primary">Access</a>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card card-primary">
          <div class="card-body">
            <i class="fa float-left fa-4x pr-5 fa-2x fa-users"></i><p>Users</p>
          </div>
          <div class="card-footer">
            <a href="{{ route('businesses.index') }}" class="btn btn-primary">Access</a>
          </div>
        </div>
      </div>
    </div>




                </div>
            </div>
@endsection
