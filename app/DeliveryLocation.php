<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryLocation extends Model
{
  public  $fillable = ['zone_id','venue_id'];

  public function venue(){
    return $this->belongsTo('\App\Venue');
  }

  public function zone(){
    return $this->belongsTo('\App\Zone');
  }
}
