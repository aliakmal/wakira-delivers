<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator, Storage;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UsersController extends Controller
{
    protected $user;

    public function __construct(\App\User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        $accounts = $this->account->query();

        $input = Input::all();

        $accounts = $accounts->paginate(20);

        return View::make('users.index', compact('accounts'));
    }


    public function create()
    {
        $roles = Role::select()->pluck('name', 'id')->all();
        return  view('users.create');
    }

    public function store(Request $request)
    {

        $data = Input::only(User::$fields);
        $all_data = Input::all();

        $rules = [
            'name' => 'required',
            'email'      => 'required|email|unique:users,email',
            'password'   => 'required', 
            'role' => 'required', 
        ];

        $validation = Validator::make($all_data, User::$rules);

        if($validation->passes()):

            $user =  $this->account->create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password'])
            ]);

            if ($user->id) {

                $user->confirmed = 1;
                $user->save();
                $user->update($data);
                $user->assignRole($all_data['role']);
                return redirect()->route('users.index');

            }else{

                $error = $user->errors()->all(':message');

                return redirect()->route('users.create')
                        ->withInput(Input::except('password'));
            }
        else:

            return redirect()->back()->withInput(Input::except('password'))
                    ->withErrors($validation);
        endif;
    }

    public function show($id)
    {
        $user = $this->account->find($id);

        if (is_null($user))
        {
          return redirect()->route('users.index');
        }

        return  view('users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = $this->account->find($id);

        if (is_null($user))
        {
          return redirect()->route('users.index');
        }

        return  view('users.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $user = $this->account->find($id);

        $input = Input::only(\App\Business::$fields);
        $validation = Validator::make($input, \App\Business::$rules);

        if ($validation->passes()){
            $input['slug'] = str_slug($input['name']);
            
            $user->fill($input);
            $user->save();

            return redirect()->route('users.show', $user);
        }

        return redirect()->route('users.edit')
                ->withInput()
                ->withErrors($validation)
                ->with('message', 'There were validation errors.');
    }

    public function destroy($id)
    {
        $this->account->find($id)->delete();

        return redirect()->route('users.index');
    }
}
