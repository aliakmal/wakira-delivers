<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator, Storage;

class CitiesController extends Controller
{
    protected $city;

    public function __construct(\App\City $city)
    {
        $this->city = $city;
    }

    public function index()
    {
        $cities = $this->city->query();
        $cities = $cities->orderby('name', 'asc')->paginate(20);

        return view('cities.index', compact('cities'));
    }

    public function create(Request $request)
    {
        $params = $request->all('bid');
        if(is_null($params['bid'])){
            return redirect()->back();
        }

        $business = \App\Business::find($params['bid']);
        if(!$business){
            return redirect()->back();
        }

        return  view('cities.create', compact('business'));
    }

    public function store(Request $request)
    {
        $input = Input::only(\App\city::$fields);
        $validation = Validator::make($input, \App\city::$rules);

        if ($validation->passes()){
            $input['slug'] = str_slug($input['name']);
            $city = $this->city->create($input);
            return redirect()->route('businesses.show', ['id'=>$city->business_id]);
        }
        return redirect()->route('cities.create')
                ->withInput()
                ->withErrors($validation)
                ->with('message', 'There were validation errors.');
    }

    public function show($id)
    {
        $city = $this->city->find($id);

        if (is_null($city))
        {
          return redirect()->route('cities.index');
        }

        $zones = $city->zones;

        return  view('cities.show', compact('city', 'zones'));
    }

    public function edit($id)
    {
        $city = $this->city->find($id);

        if (is_null($city)){
          return redirect()->route('cities.index');
        }

        $business = $city->business;

        return  view('cities.edit', compact('city', 'business'));
    }

    public function update(Request $request, $id)
    {
        $city = $this->city->find($id);

        $input = Input::only(\App\city::$fields);
        $validation = Validator::make($input, \App\city::$rules);

        if ($validation->passes()){
            $input['slug'] = str_slug($input['name']);
            
            $city->fill($input);
            $city->save();

            return redirect()->route('cities.show', $city);
        }

        return redirect()->route('cities.edit')
                ->withInput()
                ->withErrors($validation)
                ->with('message', 'There were validation errors.');
    }

    public function destroy($id)
    {
        $this->city->find($id)->delete();

        return redirect()->route('cities.index');
    }
}
