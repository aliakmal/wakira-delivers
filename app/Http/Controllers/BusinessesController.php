<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator, Storage;

class BusinessesController extends Controller
{
    protected $business;

    public function __construct(\App\Business $business)
    {
        $this->business = $business;
    }

    public function index()
    {

        $businesses = $this->business->query();
        $businesses = $businesses->orderby('name', 'asc')->paginate(5);

        return view('businesses.index', compact('businesses'));
    }

    public function create()
    {
        return  view('businesses.create');
    }

    public function store(Request $request)
    {
        $input = Input::only(\App\Business::$fields);
        $validation = Validator::make($input, \App\Business::$rules);

        if ($validation->passes()){
            $input['slug'] = str_slug($input['name']);
            $business = $this->business->create($input);
            return redirect()->route('businesses.index');
        }
        return redirect()->route('businesses.create')
                ->withInput()
                ->withErrors($validation)
                ->with('message', 'There were validation errors.');
    }

    public function show($id)
    {
        $business = $this->business->find($id);

        if (is_null($business))
        {
          return redirect()->route('businesses.index');
        }

        return  view('businesses.show', compact('business'));
    }

    public function edit($id)
    {
        $business = $this->business->find($id);

        if (is_null($business))
        {
          return redirect()->route('businesses.index');
        }

        return  view('businesses.edit', compact('business'));
    }

    public function update(Request $request, $id)
    {
        $business = $this->business->find($id);

        $input = Input::only(\App\Business::$fields);
        $validation = Validator::make($input, \App\Business::$rules);

        if ($validation->passes()){
            $input['slug'] = str_slug($input['name']);
            
            $business->fill($input);
            $business->save();

            return redirect()->route('businesses.show', $business);
        }

        return redirect()->route('businesses.edit')
                ->withInput()
                ->withErrors($validation)
                ->with('message', 'There were validation errors.');
    }

    public function destroy($id)
    {
        $this->business->find($id)->delete();

        return redirect()->route('businesses.index');
    }
}
