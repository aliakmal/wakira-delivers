<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator, Storage;

class VenuesController extends Controller
{
    protected $venue;

    public function __construct(\App\Venue $venue)
    {
        $this->venue = $venue;
    }

    public function index()
    {
        $venues = $this->venue->query();
        $venues = $venues->orderby('name', 'asc')->paginate(5);

        return view('venues.index', compact('venues'));
    }

    public function create(Request $request)
    {
        $params = $request->all('bid');
        if(is_null($params['bid'])){
            return redirect()->back();
        }

        $business = \App\Business::find($params['bid']);
        if(!$business){
            return redirect()->back();
        }

        return  view('venues.create', compact('business'));
    }

    public function store(Request $request)
    {
        $input = Input::only(\App\Venue::$fields);
        $validation = Validator::make($input, \App\Venue::$rules);

        if ($validation->passes()){
            $input['slug'] = str_slug($input['name']);
            $venue = $this->venue->create($input);
            return redirect()->route('businesses.show', ['id'=>$venue->business_id]);
        }
        return redirect()->route('venues.create')
                ->withInput()
                ->withErrors($validation)
                ->with('message', 'There were validation errors.');
    }

    public function show($id)
    {
        $venue = $this->venue->find($id);

        if (is_null($venue))
        {
          return redirect()->route('venues.index');
        }

        $business = $venue->business;

        return  view('venues.show', compact('venue', 'business'));
    }

    public function edit($id)
    {
        $venue = $this->venue->find($id);

        if (is_null($venue)){
          return redirect()->route('venues.index');
        }

        $business = $venue->business;

        return  view('venues.edit', compact('venue', 'business'));
    }

    public function update(Request $request, $id)
    {
        $venue = $this->venue->find($id);

        $input = Input::only(\App\Venue::$fields);
        $validation = Validator::make($input, \App\Venue::$rules);

        if ($validation->passes()){
            $input['slug'] = str_slug($input['name']);
            
            $venue->fill($input);
            $venue->save();

            return redirect()->route('venues.show', $venue);
        }

        return redirect()->route('venues.edit')
                ->withInput()
                ->withErrors($validation)
                ->with('message', 'There were validation errors.');
    }

    public function destroy($id)
    {
        $this->venue->find($id)->delete();

        return redirect()->route('venues.index');
    }



    public function getTimings($id)
    {
        $venue = $this->venue->find($id);
        if(!isset($venue->id)){
          return $venue; 
        }

        return view('venues.timings', compact('venue'));
    }


    public function postTimings($id)
    {
        $venue = $this->venue->find($id);
        if(!isset($venue->id)){
          return $venue; 
        }

        $timings = Input::only('timings');
        $venue->saveTimings($timings['timings']);

        return redirect()->route('venues.show', $id);

    }
    public function getDeliveryLocations($id)
    {
        $venue = $this->venue->find($id);
        if(!isset($venue->id)){
          return $venue; 
        }

        return view('venues.delivery_locations', compact('venue'));
    }


    public function postDeliveryLocations($id)
    {
        $venue = $this->venue->find($id);
        if(!isset($venue->id)){
          return $venue; 
        }

        $input = Input::only('delivery_locations');
        $venue->saveDeliveryLocations($input['delivery_locations']);

        return redirect()->route('venues.show', $id);

    }





}
