<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //
  public static $fields = ['name', 'business_id', 'currency', 'description'];

  public function business(){
    return $this->belongsTo(\App\Business::class);
  }

  public function sections(){
    return $this->hasMany(\App\MenuSection::class);
  }
}
