<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{

  public static $rules = ['name'=>'required', 'type'=>'required'];
  public static $fields = ['name', 'type', 'slug', 'desc'];
  
  protected $guarded = [];

  public function venues(){
    return $this->hasMany('\App\Venue');
  }

  public function menus(){
    return $this->hasMany('\App\Menu');
  }

}
