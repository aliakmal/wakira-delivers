<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{

  public static $rules = ['name'=>'required', 'state'=>'required'];
  public static $fields = ['name','slug','address_1','address_2','city','state','country','has_delivery','business_id'];
 
  protected $guarded = [];

  public function business(){
    return $this->belongsTo('\App\Business');
  }

  public function delivery_locations(){
    return $this->hasMany('\App\DeliveryLocation');
  }

  public function timings(){
    return $this->hasMany('\App\Timing');
  }

  public function getFormattedAddress(){
    return join(',', array_filter([$this->address_1, $this->address_2, $this->city, $this->state, $this->country]));
  }

  public function hasDelivery(){
    return $this->has_delivery == 1 ? true : false;
  }

  public function saveDeliveryLocations($locations){
    \App\DeliveryLocation::select()->where('venue_id', '=', $this->id)->whereNotIn('zone_id', $locations)->delete();
    $existing = \App\DeliveryLocation::select()->where('venue_id', '=', $this->id)->whereIn('zone_id', $locations)->pluck('zone_id', 'zone_id')->all();
    foreach($locations as $location):
      if(in_array($location, $existing)){
        continue;
      }
      $data = array('zone_id'=>$location, 'venue_id'=>$this->id);
      \App\DeliveryLocation::create($data);
    endforeach;

  }


  public function saveTimings($timings){
    if(!is_array($timings)){
      return false;
    }
    foreach($timings as $timing):

      if(isset($timing['id']) && ($timing['id'] > 0)){ // if we have an id so consider this is an update request

        $t = \App\Timing::find($timing['id']);

        // if this has been marked for deletion 
        if($t):
          if(isset($timing['deletable']) && ($timing['deletable'] > 0)){
            $t->delete($timing['id']);  // delete it
          }else{              // else
            unset($timing['deletable']);
            $t->update($timing);  // update it
          }
        endif;
      }else{
        unset($timing['deletable']);
        $this->timings()->save(new \App\Timing($timing)); // save a new timing
      }
    endforeach;
  } 


}
