<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timing extends Model
{
  public static $fields = ['days_of_week','open','close','venue_id'];
  public  $fillable = ['days_of_week','open','close','venue_id'];

}
