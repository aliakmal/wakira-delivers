<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call(ZonesSeeder::class);
        // $this->call(UsersTableSeeder::class);
    }
}
