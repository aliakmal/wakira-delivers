<?php

use Illuminate\Database\Seeder;

class ZonesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $handle = fopen(storage_path('zones.csv'), "r");
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {      
        $city_data = ['name'=>$data[0], 'slug'=>str_slug($data[0]), 'country'=>'AE'];
        $city = \App\City::firstOrCreate($city_data);

        $zone_data = ['name'=>$data[1], 'slug'=>$data[2], 'city_id'=>$city->id];
        $zone = \App\Zone::firstOrCreate($zone_data);
        $this->command->line($zone->id.' - '.$zone->name.' - ['.$city->name.'] set in the database');/**/
      }

      $this->command->line('All done :)');
    }
}
