<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //
      $role_names = ['administrator', 'manager', 'customer'];
      foreach($role_names as $name){
        $role = Role::firstOrCreate(['name'=>$name]);
      }

      $admin_user = \App\User::where('email', '=', 'admin@wakira.com')->first();
      $admin_user->assignRole('administrator');

    }
}
