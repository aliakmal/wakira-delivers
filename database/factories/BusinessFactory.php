<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Business::class, function (Faker $faker) {
    return [
      'name'=>$faker->company,
      'type'=>$faker->randomElement(['restaurant', 'cafe']),
      'slug'=>$faker->slug,
      'desc'=>$faker->realText(200,2),
    ];
});
