<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MenuModelTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    use DatabaseTransactions;

    public function test_menu_must_belong_to_a_business(){
      $business = factory(\App\Business::class)->create();
      $menu = factory(\App\Menu::class)->create(['business_id'=>$business->id]);
      $this->assertEquals($menu->business_id, $business->id);
    }

    public function test_business_can_have_multiple_menus(){
      $menu_count = 10;
      $business = factory(\App\Business::class)->create();
      $menu = factory(\App\Menu::class, $menu_count)->create(['business_id'=>$business->id]);

      $this->assertCount($menu_count, $business->menus);
    }

    public function test_menu_has_multiple_sections(){
      $menu = factory(\App\Menu::class)->create();
      $section_count = 5;
      $sections = factory(\App\MenuSection::class, $section_count)->create(['menu_id'=>$menu->id]);

      $this->assertCount($section_count, $menu->sections);
    }



}
